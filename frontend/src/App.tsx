import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import UserListView from 'components/users/UserList/UserList'


function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    {/* TODO */}
                    <Route path='/' component={UserListView} />
                    <Route path='/users' exact component={UserListView} />
                </Switch>

            </BrowserRouter>
        </div>
    );
}

export default App;
