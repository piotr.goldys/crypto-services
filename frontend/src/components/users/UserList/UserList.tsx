import Table from "react-bootstrap/Table";
import React, {useEffect, useState} from "react";
import './UserList.css';


interface User {
    id: number;
    username: string;
    date_joined: "string";
    "avatar": string;

}

function UserListView() {
    const [users, setUsers] = useState([]);
    useEffect(() => {
        (
            async () => {
                const response = await fetch('http://localhost:1337/users');

                const data = await response.json();
                console.log('RESPONSE', data)

                setUsers(data);
            }
        )();
    }, []);


    return (
        <>
            <div className="users-service">
                <Table striped bordered>
                    <thead>
                    <tr>
                        <th>#ID</th>
                        <th>Username</th>
                        <th>Avatar</th>
                        <th>Date joined</th>
                    </tr>
                    </thead>
                    <tbody>
                        {users.map(
                            (user: User) => {
                                return (
                                    <tr>
                                        <td>{user.id}</td>
                                        <td>{user.username}</td>
                                        <td><img src={'http://localhost:1337' + user.avatar} /></td>
                                        <td>{user.date_joined}</td>
                                    </tr>
                                )
                            }
                        )}
                    </tbody>
                </Table>
            </div>
        </>
    )
}

export default UserListView;
