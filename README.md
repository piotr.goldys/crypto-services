
>> ### Disclaimer: This project is still under construction, however you are free to explore backend side already.

[![Python: 3.9](https://img.shields.io/badge/python-3.9-blue)](https://www.python.org/downloads/release/python-397/})
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Pre-commit: enabled](https://img.shields.io/badge/pre--commit-enabled-brightgreen)](https://pre-commit.com/)


# Summary
I created this project mostly to get better practical grasp of microservice architecture and showcase my backend skills.


## Architecture &nbsp;&nbsp;&nbsp; <img src="./assets/microservices-logo.png" width="80"> &nbsp; <img src="./assets/docker-logo.png" width="50"> &nbsp; <img src="./assets/nginx-logo.png" width="80"> &nbsp; <img src="./assets/gunicorn-logo.png" width="80">
Project is developed with **microservice** architecture in mind.  
Each service is a separate entity decoupled from the others.  
Services are containerized using **Docker**, each one having its own docker-compose configuration.   
Moreover, services are running separately with **Gunicorn**, behind **Nginx** server/reverse-proxy which can be also used as a load balancer if needed.  
  
Project components are stored in a single monorepo for easy “glance” and showcase of the whole project, however they are ready to be decoupled and deployed on separate machines.  

## Frameworks &nbsp;&nbsp;&nbsp; <img src="./assets/django-logo.png" width="35"> &nbsp;&nbsp;&nbsp; <img src="./assets/flask-logo.png" width="28"> &nbsp; <img src="./assets/fastapi-logo.png" width="45"> &nbsp; <img src="./assets/react-logo.png" width="35">
* Django
* Flask
* FastAPI
* React


## Caching &nbsp;&nbsp;&nbsp; <img src="./assets/redis-logo-horizontal.svg" width="100">
Some API responses are cached server-side using Redis in-memory cache engine.  
This enabled optimising response times for endpoints returning data which is less prone to changes.

## Logging &nbsp;&nbsp;&nbsp; <img src="./assets/elasticsearch-logo-horizontal.png" width="130"> &nbsp; <img src="./assets/logstash-logo-horizontal.png" width="90"> &nbsp;&nbsp; <img src="./assets/kibana-logo-horizontal.svg" width="80">
ELK stack (Elasticsearch, Logstash, Kibana) enables gathering, storing and visualizing logs produced by each service.   
Logs are sent in an asynchronous manner.

## Distributed tracing &nbsp;&nbsp;&nbsp; 
In order to trace business logic through multiple distributed and decoupled services,  
Elastic APM Server is used in conjunction with Elasticsearch and Kibana.  
By assigning trace ID to each log, we are able to track correlated logs through multiple services.  
<img src="./assets/network.jpg" width="800"> 
<img src="./assets/traces.jpg" width="700">

## Monitoring &nbsp;&nbsp;&nbsp; 
#### Kibana
Used to explore and visualize different metrics:  
<img src="./assets/charts.jpg" width="700">  
<img src="./assets/services.jpg" width="700">
<br><br>
#### Flower 
Enables task queue and execution monitoring:  
<img src="./assets/flower-dashboard.jpg" width="700">  
<img src="./assets/flower-tasks.jpg" width="700">

## Tests &nbsp;&nbsp;&nbsp; 
Pytest is used for unit-testing each service on its own, as well as correlations between them (integration tests).

## Frontend &nbsp;&nbsp;&nbsp; 
Separate React service is used for frontend. Currently in progress...

## Gitlab CI <img src="./assets/black-logo.png" width="100">
Gitlab CI pipelines are configured to ensure code compliance with pre-commit (black formatting, linting, etc.),  
as well as pytest tests passing for each service.
<img src="./assets/gitlab-pipelines.jpg" width="700">



## Ports used


<table>
  <thead>
    <tr align="center">
      <th>Port</th>
      <th>Expose</th>
      <th>Resource</th>
      <th>Production</th>
    </tr>
  </thead>
  <tbody>
    <tr align="center">
      <th colspan=4>frontend</th>
    </tr>
    <tr>
      <td><b>80</b></td>
      <td>Public</td>
      <td>Nginx</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>3000</b></td>
      <td>Internal</td>
      <td>React Frontend</td>
      <td align="center">No (local only)</td>
    </tr>
    <tr align="center">
      <th colspan=4>elasticsearch</th>
    </tr>
    <tr>
      <td><b>9200</b></td>
      <td>Public</td>
      <td>Elasticsearch API</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>9300</b></td>
      <td>Internal</td>
      <td>Elasticsearch</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>logstash</th>
    </tr>
    <tr>
      <td><b>5100</b></td>
      <td>Public</td>
      <td>Logstash Listener</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>5044</b></td>
      <td>Public</td>
      <td>Logstash Beats</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>9600</b></td>
      <td>Internal</td>
      <td>Logstash API</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>kibana</th>
    </tr>
    <tr>
      <td><b>5601</b></td>
      <td>Public</td>
      <td>Kibana</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>apm_server</th>
    </tr>
    <tr>
      <td><b>8200</b></td>
      <td>Public</td>
      <td>Elastic APM Server</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>users</th>
    </tr>
    <tr>
      <td><b>81</b></td>
      <td>Public</td>
      <td>Nginx</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>33066</b></td>
      <td>Public</td>
      <td>MySQL Database</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>8000</b></td>
      <td>Internal</td>
      <td>Django Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>3066</b></td>
      <td>Internal</td>
      <td>MySQL Database</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>redis</th>
    </tr>
    <tr>
      <td><b>6379</b></td>
      <td>Public</td>
      <td>Redis Cache</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>portfolio_results</th>
    </tr>
    <tr>
      <td><b>82</b></td>
      <td>Public</td>
      <td>Flask Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>5000</b></td>
      <td>Internal</td>
      <td>Flask Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>currency_converter</th>
    </tr>
    <tr>
      <td><b>83</b></td>
      <td>Public</td>
      <td>FastAPI Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>6000</b></td>
      <td>Internal</td>
      <td>FastAPI Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>newsfeed</th>
    </tr>
    <tr>
      <td><b>84</b></td>
      <td>Public</td>
      <td>Nginx</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>33067</b></td>
      <td>Public</td>
      <td>MySQL Database</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>8888</b></td>
      <td>Public</td>
      <td>Flower</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>8001</b></td>
      <td>Internal</td>
      <td>Django Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>3307</b></td>
      <td>Internal</td>
      <td>MySQL Database</td>
      <td align="center">Yes</td>
    </tr>
    <tr align="center">
      <th colspan=4>videofeed</th>
    </tr>
    <tr>
      <td><b>85</b></td>
      <td>Public</td>
      <td>Nginx</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>33068</b></td>
      <td>Public</td>
      <td>MySQL Database</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>8889</b></td>
      <td>Public</td>
      <td>Flower</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>8002</b></td>
      <td>Internal</td>
      <td>Django Backend</td>
      <td align="center">Yes</td>
    </tr>
    <tr>
      <td><b>3308</b></td>
      <td>Internal</td>
      <td>MySQL Database</td>
      <td align="center">Yes</td>
    </tr>
  </tbody>
</table>

## TODO
* React frontend
* Unit tests: newsfeed, videofeed, saved_feed
* More integration tests
* Deployment
* Redis cache pre-populate (asynchronous task)
* Better external API error handling
