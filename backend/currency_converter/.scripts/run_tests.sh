#!/bin/sh

set -o errexit
set -o nounset

echo "Running tests..."

[ ! "$(docker ps -a | grep redis)" ] && echo "You must run redis instance before tests!"

# Create main network if it does not exist.
docker network inspect crypto-services >/dev/null || docker network create crypto-services

# We run 'python -m pytest' instead of 'pytest' because of PYTHONPATH issues.
# Python sets PYTHONPATH to current directory automatically.
docker-compose run --rm currency_converter_internal python3 -m pytest tests/
