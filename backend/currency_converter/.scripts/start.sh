#!/bin/sh

set -o errexit
set -o nounset

gunicorn -k uvicorn.workers.UvicornWorker app.main:app --bind 0.0.0.0:6000
