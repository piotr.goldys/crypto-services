from datetime import date
from datetime import datetime
from typing import Optional

import requests
from app.choices import get_fiat_currency_choices
from app.constants import CURRENCY_EXCHANGE_API_URL
from fastapi import APIRouter
from pydantic import BaseModel
from pydantic import Field
from pydantic import ValidationError
from pydantic import validator

router = APIRouter()

# Use session for optimization.
session = requests.Session()


class ConversionData(BaseModel):
    currency: str = Field(regex='[A-Z]')  # Allow only uppercase.
    date: Optional[date]
    amount: int

    @validator('date', pre=True)
    def validate_date_format(cls, val):
        """Custom date validation to enforce specific format and customize error messages."""
        try:
            val = datetime.strptime(val, '%Y-%m-%d').date()

            if not (date(year=1999, month=1, day=4) < val <= date.today()):
                raise ValidationError(f'Specified date must be between 1999-01-01 and {date.today}.')

            return val

        except ValueError:
            raise ValueError(
                'Invalid date format. Please provide dates in YYYY-MM-DD format, eg. 2020-07-18.'
            )

    @validator('currency')
    def validate_currency(cls, val):
        allowed_currencies = [currency['symbol'] for currency in get_fiat_currency_choices()['choices']]
        if val not in allowed_currencies:
            raise ValidationError('Currency is not supported.')
        return val


@router.post('/convert-to-usd')
def convert_to_usd(data: ConversionData) -> float:
    """
    Get value of specific currency in USD for given date.
    If date is not provided, convert for current day.
    """
    date_param = data.date or 'latest'
    response_data = session.get(
        f'{CURRENCY_EXCHANGE_API_URL}/{date_param}?from={data.currency}&to=USD&amount={data.amount}'
    ).json()

    return round(response_data['rates']['USD'], 2)


@router.post('/convert-from-usd')
def convert_from_usd(data: ConversionData) -> float:
    """
    Get value of specified amount of USD in given currency, for given date.
    If date is not provided, convert for current day.
    """
    return round(data.amount / (convert_to_usd(data) / data.amount), 2)
