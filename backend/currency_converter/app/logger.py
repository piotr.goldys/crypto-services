import logging
import os

from logstash_async.handler import AsynchronousLogstashHandler

logger = logging.getLogger('currency_converter')
logger.setLevel(logging.DEBUG)  # Default level is info.
async_handler = AsynchronousLogstashHandler(
    host=os.environ['LOGSTASH_HOST_NAME'],
    port=int(os.environ['LOGSTASH_PORT']),
    database_path=None,  # Do not store logs in any database, before they are sent.
)
logger.addHandler(async_handler)
