import requests

from app.constants import CURRENCY_EXCHANGE_API_URL
from app.redis import cache
from fastapi import APIRouter


router = APIRouter()


@router.get('/get-fiat-currency-choices')
@cache.list(ttl=60 * 60)
def get_fiat_currency_choices():
    response_data = requests.get(f'{CURRENCY_EXCHANGE_API_URL}/currencies').json()

    choices = []
    for symbol, name in response_data.items():
        choices.append(
            {
                'symbol': symbol,
                'name': name,
            }
        )

    return {'choices': choices}
