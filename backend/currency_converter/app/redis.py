from redis_dec import Cache

from redis import StrictRedis

client = StrictRedis(host='redis', decode_responses=True)
cache = Cache(client)
