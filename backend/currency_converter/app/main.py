import os

from app.choices import router as choices
from app.conversion import router as conversion
from elasticapm.contrib.starlette import ElasticAPM
from elasticapm.contrib.starlette import make_apm_client
from fastapi import FastAPI

app = FastAPI()

apm = make_apm_client({'SERVICE_NAME': 'currency_converter', 'SERVER_URL': os.environ['APM_SERVER_URL']})
app.add_middleware(ElasticAPM, client=apm, logging=True)


app.include_router(choices)
app.include_router(conversion)


@app.get('/')
def index():
    return {'Hello': 'World'}
