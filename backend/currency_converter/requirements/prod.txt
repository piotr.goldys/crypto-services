python-logstash-async==2.3.0
elastic-apm==6.3.3
ecs-logging==1.0.1

requests==2.26.0
redis-decorator==0.4
