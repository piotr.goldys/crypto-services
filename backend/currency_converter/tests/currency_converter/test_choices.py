from unittest.mock import Mock
from unittest.mock import patch

from app.constants import CURRENCY_EXCHANGE_API_URL
from tests.currency_converter.constants import GET_FIAT_CURRENCY_CHOICES_ENDPOINT_EXAMPLE_RESPONSE


@patch('requests.get')
def test_get_fiat_currency_choices(mock, client):
    mock.return_value = Mock(ok=True)
    mock.return_value.json.return_value = {
        'EUR': 'Euro',
        'PLN': 'Polish Złoty',
        'USD': 'United States Dollar',
    }

    response = client.get('/get-fiat-currency-choices')

    assert response.json() == GET_FIAT_CURRENCY_CHOICES_ENDPOINT_EXAMPLE_RESPONSE

    assert mock.call_count in (0, 1)  # If response is cached, external API is not called.
    if mock.call_count == 1:
        mock.assert_called_once_with(f'{CURRENCY_EXCHANGE_API_URL}/currencies')
