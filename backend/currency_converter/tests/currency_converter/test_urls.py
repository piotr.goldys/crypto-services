from app.main import app


class TestAPIURLs:
    """Test API URL backward compatibility (make sure URLs don't change)."""

    urls = [route.path for route in app.routes]

    def test_get_fiat_currency_choices_url(self):
        assert '/get-fiat-currency-choices' in self.urls

    def test_convert_to_usd_url(self):
        assert '/convert-to-usd' in self.urls
