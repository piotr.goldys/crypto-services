API_RESPONSE_MOCK = {'amount': 1000.0, 'base': 'PLN', 'date': '2021-08-06', 'rates': {'USD': 333.01}}

EXAMPLE_CONVERSION_REQUEST_BODY_WITH_DATE = {
    'currency': 'PLN',
    'date': '2020-07-18',
    'amount': 1000,
}

EXAMPLE_CONVERSION_REQUEST_BODY_WITHOUT_DATE = {
    'currency': 'PLN',
    'amount': 1000,
}

# Wrong date format.
WRONG_BODY_1 = {
    'currency': 'PLN',
    'date': '97-07-18',
    'amount': 1000,
}

# Wrong date format.
WRONG_BODY_2 = {
    'currency': 'PLN',
    'date': '18-07-2020',  # Date must be in YYY-MM-DD format.
    'amount': 1000,
}

# Currency missing (with date).
WRONG_BODY_3 = {
    'date': '2020-07-18',
    'amount': 1000,
}

# Currency missing (without date).
WRONG_BODY_4 = {
    'amount': 1000,
}

# Amount missing (with date).
WRONG_BODY_5 = {
    'currency': 'PLN',
    'date': '2020-07-18',
}

# Amount missing (without date).
WRONG_BODY_6 = {
    'currency': 'PLN',
}

# Empty.
WRONG_BODY_7 = {}

# Date too early.
WRONG_BODY_8 = {
    'currency': 'PLN',
    'date': '1990-07-18',
    'amount': 1000,
}

# Date in the future.
WRONG_BODY_9 = {
    'currency': 'PLN',
    'date': '2100-07-18',
    'amount': 1000,
}

# Lowercase currency.
WRONG_BODY_10 = {
    'currency': 'pln',
    'date': '2020-07-18',
    'amount': 1000,
}


GET_FIAT_CURRENCY_CHOICES_ENDPOINT_EXAMPLE_RESPONSE = {
    'choices': [
        {'symbol': 'EUR', 'name': 'Euro'},
        {'symbol': 'PLN', 'name': 'Polish Złoty'},
        {'symbol': 'USD', 'name': 'United States Dollar'},
    ]
}
