from http.client import UNPROCESSABLE_ENTITY
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from app.constants import CURRENCY_EXCHANGE_API_URL
from tests.currency_converter.constants import API_RESPONSE_MOCK
from tests.currency_converter.constants import EXAMPLE_CONVERSION_REQUEST_BODY_WITH_DATE
from tests.currency_converter.constants import EXAMPLE_CONVERSION_REQUEST_BODY_WITHOUT_DATE
from tests.currency_converter.constants import GET_FIAT_CURRENCY_CHOICES_ENDPOINT_EXAMPLE_RESPONSE
from tests.currency_converter.constants import WRONG_BODY_1
from tests.currency_converter.constants import WRONG_BODY_10
from tests.currency_converter.constants import WRONG_BODY_2
from tests.currency_converter.constants import WRONG_BODY_3
from tests.currency_converter.constants import WRONG_BODY_4
from tests.currency_converter.constants import WRONG_BODY_5
from tests.currency_converter.constants import WRONG_BODY_6
from tests.currency_converter.constants import WRONG_BODY_7
from tests.currency_converter.constants import WRONG_BODY_8
from tests.currency_converter.constants import WRONG_BODY_9


@patch('app.conversion.get_fiat_currency_choices')
def _test_wrong_request_body(client, body, choices_mock, endpoint_url):
    choices_mock.return_value = GET_FIAT_CURRENCY_CHOICES_ENDPOINT_EXAMPLE_RESPONSE

    response = client.post(endpoint_url, json=body)

    assert response.status_code == UNPROCESSABLE_ENTITY
    assert choices_mock.call_count in (0, 1)  # Validation can fail before the call.


@patch('app.conversion.get_fiat_currency_choices')
@patch('app.conversion.session.get')
def _test_success(client, request_mock, choices_mock, endpoint_url, request_body, expected_return):
    choices_mock.return_value = GET_FIAT_CURRENCY_CHOICES_ENDPOINT_EXAMPLE_RESPONSE

    request_mock.return_value = Mock(ok=True)
    request_mock.return_value.json.return_value = API_RESPONSE_MOCK

    response = client.post(
        endpoint_url,
        json=request_body,
    )

    date_param = request_body.get('date') or 'latest'

    assert response.json() == expected_return
    choices_mock.assert_called_once()
    request_mock.assert_called_once_with(
        f'{CURRENCY_EXCHANGE_API_URL}/{date_param}?from=PLN&to=USD&amount=1000'
    )


class TestConvertToUSD:

    # This test shouldn't use requests, because it will fail validation earlier.
    @pytest.mark.parametrize(
        'body',
        (
            WRONG_BODY_1,
            WRONG_BODY_2,
            WRONG_BODY_3,
            WRONG_BODY_4,
            WRONG_BODY_5,
            WRONG_BODY_6,
            WRONG_BODY_7,
            WRONG_BODY_8,
            WRONG_BODY_9,
            WRONG_BODY_10,
        ),
    )
    def test_wrong_request(self, client, body):
        _test_wrong_request_body(client, body, endpoint_url='/convert-to-usd')

    def test_success_historical(self, client):
        _test_success(
            client,
            endpoint_url='/convert-to-usd',
            request_body=EXAMPLE_CONVERSION_REQUEST_BODY_WITH_DATE,
            expected_return=333.01,
        )

    def test_success_current(self, client):
        _test_success(
            client,
            endpoint_url='/convert-to-usd',
            request_body=EXAMPLE_CONVERSION_REQUEST_BODY_WITHOUT_DATE,
            expected_return=333.01,
        )


class TestConvertFromUSD:

    # This test shouldn't use requests, because it will fail validation earlier.
    @pytest.mark.parametrize(
        'body',
        (
            WRONG_BODY_1,
            WRONG_BODY_2,
            WRONG_BODY_3,
            WRONG_BODY_4,
            WRONG_BODY_5,
            WRONG_BODY_6,
            WRONG_BODY_7,
            WRONG_BODY_8,
            WRONG_BODY_9,
            WRONG_BODY_10,
        ),
    )
    def test_wrong_request(self, client, body):
        _test_wrong_request_body(client, body, endpoint_url='/convert-from-usd')

    def test_success_historical(self, client):
        _test_success(
            client,
            endpoint_url='/convert-from-usd',
            request_body=EXAMPLE_CONVERSION_REQUEST_BODY_WITH_DATE,
            expected_return=3002.91,
        )

    def test_success_current(self, client):
        _test_success(
            client,
            endpoint_url='/convert-from-usd',
            request_body=EXAMPLE_CONVERSION_REQUEST_BODY_WITHOUT_DATE,
            expected_return=3002.91,
        )
