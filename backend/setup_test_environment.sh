#!/bin/sh

# Create global network if does not exist.
docker network inspect crypto-services >/dev/null || docker network create crypto-services

# Run redis instance
docker-compose -f redis/docker-compose.yml up -d
