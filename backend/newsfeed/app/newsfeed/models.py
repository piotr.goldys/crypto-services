from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import ImageField
from django.db.models import Model
from django.db.models import URLField


class Article(Model):
    url = URLField(unique=True)
    title = CharField(max_length=255)
    publication_date = DateTimeField()
    image = ImageField(upload_to='article_images')
