from app.newsfeed.models import Article
from app.newsfeed.serializers import ArticleSerializer
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet


class ArticleViewSet(ViewSet):
    def retrieve(self, request, pk):
        user = get_object_or_404(Article, pk=pk)
        serializer = ArticleSerializer(user)
        return Response(serializer.data)

    def list(self, request):
        users = Article.objects.all()
        serializer = ArticleSerializer(users, many=True)
        return Response(serializer.data)
