from functools import cached_property
from io import BytesIO
from uuid import uuid4

import requests
from app.celery import app
from app.logger import logger
from app.newsfeed.constants import REQUESTS_HEADERS
from app.newsfeed.constants import YAHOO_FINANCE_CRYPTO_NEWS_URL
from app.newsfeed.constants import YAHOO_FINANCE_URL
from app.newsfeed.helpers import get_image_extension
from app.newsfeed.models import Article
from bs4 import BeautifulSoup
from django.core.files.images import ImageFile


class ArticleAdditionalInfoFetcher:
    def __init__(self, article_url):
        self.article_url = article_url

    @cached_property
    def article_soup(self):
        html = requests.get(self.article_url, headers=REQUESTS_HEADERS)
        return BeautifulSoup(html.text, 'lxml')

    def get_article_title(self):
        return self.article_soup.find('header', class_='caas-title-wrapper').h1.text

    def get_article_publication_date(self):
        return self.article_soup.find('time')['datetime']


@app.task(name='fetch_new_articles')
def fetch_new_articles():
    html = requests.get(YAHOO_FINANCE_CRYPTO_NEWS_URL)
    soup = BeautifulSoup(html.text, 'lxml')

    for article_row in soup.find_all('li', class_='js-stream-content'):
        article_url_elem = article_row.find('a', class_='js-content-viewer')

        if not article_url_elem:  # Some rows are hidden miscellaneous data.
            continue

        article_url = YAHOO_FINANCE_URL + article_url_elem['href']
        article_image_url = article_row.find('img')['src']
        image_response = requests.get(article_image_url)

        fetcher = ArticleAdditionalInfoFetcher(article_url)

        if not Article.objects.filter(url=article_url).exists():
            Article.objects.create(
                url=article_url,
                title=fetcher.get_article_title(),
                publication_date=fetcher.get_article_publication_date(),
                image=ImageFile(
                    BytesIO(image_response.content), name=str(uuid4()) + get_image_extension(image_response)
                ),
            )
            logger.info('New Article was added')
