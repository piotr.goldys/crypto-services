YAHOO_FINANCE_URL = 'https://finance.yahoo.com'
YAHOO_FINANCE_CRYPTO_NEWS_URL = f'{YAHOO_FINANCE_URL}/topic/crypto/'

# Fake User-Agent to prevent being detected as bot.
REQUESTS_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
    'Chrome/71.0.3578.98 Safari/537.36'
}
