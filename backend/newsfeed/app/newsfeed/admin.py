from app.newsfeed.models import Article
from django.contrib import admin

admin.site.register(Article)
