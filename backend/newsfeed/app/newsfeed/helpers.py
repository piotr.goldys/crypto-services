import os
from mimetypes import guess_extension
from urllib.parse import urlparse

from requests import Response


def get_image_extension(image_response: Response) -> str:
    mime_type = image_response.headers.get('Content-Type')
    ext = guess_extension(mime_type)

    # 'webp' extension is not supported in mimetypes.
    # More: https://bugs.python.org/issue38902
    if not ext and 'webp' in mime_type:
        ext = '.webp'

    if not ext:
        # Try to get extension from URL.
        x = urlparse(image_response.url)
        ext = os.path.splitext(x.path)[1]
    return ext
