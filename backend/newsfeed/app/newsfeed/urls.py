from app.newsfeed.views import ArticleViewSet
from django.urls import path

urlpatterns = [
    path(
        'articles',
        ArticleViewSet.as_view(
            {
                'get': 'list',
            }
        ),
        name='articles_list',
    ),
    path(
        'articles/<int:pk>',
        ArticleViewSet.as_view({'get': 'retrieve'}),
        name='articles_retrieve',
    ),
]
