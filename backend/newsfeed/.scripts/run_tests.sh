#!/bin/sh

set -o errexit
set -o nounset


echo "Running tests..."

[ ! "$(docker ps -a | grep redis)" ] && echo "You must run redis instance before tests!"

# Export all environment variables from env_file.
export $(xargs < .env)

# Create main network if it does not exist.
docker network inspect crypto-services >/dev/null || docker network create crypto-services

# Gitlab CI detects 'docker' as 'localhost'.
if [ "$ENVIRONMENT" = "local" ]
then
  echo "Local environment detected..."
  DATABASE="localhost:$MYSQL_PORT_LOCAL"
else
  echo "Production environment detected..."
  DATABASE="docker:$MYSQL_PORT_LOCAL"
fi

# Start db service.
docker-compose up -d db

# Wait for db to become available.
.scripts/wait_for_it.sh -t 60 $DATABASE

# Run tests.
docker-compose run -e DJANGO_SETTINGS_MODULE=app.django_settings.settings.test --rm newsfeed_internal pytest

# '--rm' argument removes the container after the run, but leaves dependencies it created.
# Thus, we need to kill them afterwards. More reading: https://github.com/docker/compose/issues/2791
docker-compose down
