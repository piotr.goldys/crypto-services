from app.videofeed.views import VideoViewSet
from django.urls import path

urlpatterns = [
    path(
        'videos',
        VideoViewSet.as_view(
            {
                'get': 'list',
            }
        ),
        name='videos_list',
    ),
    path(
        'videos/<int:pk>',
        VideoViewSet.as_view({'get': 'retrieve'}),
        name='videos_retrieve',
    ),
]
