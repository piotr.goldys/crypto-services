from app.videofeed.models import Video
from app.videofeed.models import YoutubeChannel
from rest_framework.serializers import ModelSerializer


class YoutubeChannelSerializer(ModelSerializer):
    class Meta:
        model = YoutubeChannel
        fields = ['id', 'name', 'channel_id']


class VideoSerializer(ModelSerializer):
    class Meta:
        model = Video
        fields = ['id', 'url', 'channel']
