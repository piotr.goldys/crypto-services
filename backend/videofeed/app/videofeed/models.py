from django.db.models import CASCADE
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import ForeignKey
from django.db.models import Model
from django.db.models import URLField


class YoutubeChannel(Model):
    name = CharField(max_length=128)
    channel_id = CharField(max_length=30)  # Channel ID defined by Youtube.


class Video(Model):
    url = URLField(unique=True)
    publication_date = DateTimeField()
    channel = ForeignKey(YoutubeChannel, on_delete=CASCADE)
