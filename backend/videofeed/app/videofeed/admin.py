from app.videofeed.models import Video
from app.videofeed.models import YoutubeChannel
from django.contrib import admin

admin.site.register(Video)
admin.site.register(YoutubeChannel)
