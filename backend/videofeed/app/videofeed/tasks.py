import requests
from app.celery import app
from app.logger import logger
from app.videofeed.models import Video
from app.videofeed.models import YoutubeChannel
from bs4 import BeautifulSoup
from dateutil import parser


@app.task(name='fetch_new_videos')
def fetch_new_videos():
    NEWEST_VIDEO_AMOUNT = 10
    youtube_channels = YoutubeChannel.objects.all()

    if not youtube_channels:
        logger.warning('No YoutubeChannels exist in the database.')

    for channel in YoutubeChannel.objects.all():
        html = requests.get(f'https://www.youtube.com/feeds/videos.xml?channel_id={channel.channel_id}')
        soup = BeautifulSoup(html.text, 'lxml')

        for entry in soup.find_all('entry')[:NEWEST_VIDEO_AMOUNT]:
            url = entry.find('link')['href']
            publication_date = parser.parse(entry.find('published').text)

            video, created = Video.objects.get_or_create(
                url=url, publication_date=publication_date, channel=channel
            )
            if created:
                logger.info(f'New Video was added from channel: {channel.name}')
