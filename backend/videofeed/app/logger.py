import logging
import os

from logstash_async.handler import AsynchronousLogstashHandler

from celery import signals


@signals.after_setup_logger.connect
def setup_logstash_logger(logger=None, *args, **kwargs):
    logger = logger or logging.getLogger('videofeed')
    logger.setLevel(os.environ['LOGGING_LEVEL'])
    async_handler = AsynchronousLogstashHandler(
        host=os.environ['LOGSTASH_HOST_NAME'],
        port=int(os.environ['LOGSTASH_PORT']),
        database_path=None,  # Do not store logs in any database, before they are sent.
    )
    logger.addHandler(async_handler)
    return logger


logger = setup_logstash_logger()
