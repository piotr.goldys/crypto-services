from app.saved_feed.models import SavedArticle
from app.saved_feed.models import SavedVideo
from app.saved_feed.serializers import SavedArticleSerializer
from app.saved_feed.serializers import SavedVideoSerializer
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.viewsets import ViewSet


class UserSavedVideosViewSet(ViewSet):
    """List all SavedVideo objects for given user."""

    def list(self, request, pk):
        saved_videos = SavedVideo.objects.filter(user_id=pk)
        serializer = SavedVideoSerializer(saved_videos, many=True)
        return Response(serializer.data)


class SavedVideoViewSet(ViewSet):
    def create(self, request):
        serializer = SavedVideoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=HTTP_201_CREATED)

    def delete(self, request, pk):
        get_object_or_404(SavedVideo, pk=pk).delete()
        return Response(status=HTTP_204_NO_CONTENT)


class UserSavedArticlesViewSet(ViewSet):
    """List all SavedArticle objects for given user."""

    def list(self, request, pk):
        saved_articles = SavedArticle.objects.filter(user_id=pk)
        serializer = SavedArticleSerializer(saved_articles, many=True)
        return Response(serializer.data)


class SavedArticleViewSet(ViewSet):
    def create(self, request):
        serializer = SavedArticleSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=HTTP_201_CREATED)

    def delete(self, request, pk):
        get_object_or_404(SavedArticle, pk=pk).delete()
        return Response(status=HTTP_204_NO_CONTENT)
