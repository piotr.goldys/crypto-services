from app.saved_feed.models import SavedArticle
from app.saved_feed.models import SavedVideo
from rest_framework.serializers import ModelSerializer


class SavedVideoSerializer(ModelSerializer):
    class Meta:
        model = SavedVideo
        fields = ['id', 'user_id', 'video_id']


class SavedArticleSerializer(ModelSerializer):
    class Meta:
        model = SavedArticle
        fields = ['id', 'user_id', 'article_id']
