from app.saved_feed.views import SavedArticleViewSet
from app.saved_feed.views import SavedVideoViewSet
from app.saved_feed.views import UserSavedArticlesViewSet
from app.saved_feed.views import UserSavedVideosViewSet
from django.urls import path

urlpatterns = [
    path(
        'saved-videos/<int:pk>',
        UserSavedVideosViewSet.as_view(
            {
                'get': 'list',
            }
        ),
        name='user_saved_videos_list',
    ),
    path(
        'saved-videos',
        SavedVideoViewSet.as_view({'post': 'create', 'delete': 'delete'}),
        name='saved_video_create_delete',
    ),
    path(
        'saved-articles/<int:pk>',
        UserSavedArticlesViewSet.as_view({'get': 'list'}),
        name='user_saved_articles_list',
    ),
    path(
        'saved-articles',
        SavedArticleViewSet.as_view({'post': 'create', 'delete': 'delete'}),
        name='saved_article_create_delete',
    ),
]
