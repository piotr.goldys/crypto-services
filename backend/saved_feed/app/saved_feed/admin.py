from app.saved_feed.models import SavedArticle
from app.saved_feed.models import SavedVideo
from django.contrib import admin

admin.site.register(SavedVideo)
admin.site.register(SavedArticle)
