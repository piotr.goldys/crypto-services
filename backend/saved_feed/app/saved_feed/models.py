from django.db.models import Model
from django.db.models import PositiveIntegerField


class SavedVideo(Model):
    user_id = PositiveIntegerField()
    video_id = PositiveIntegerField()


class SavedArticle(Model):
    user_id = PositiveIntegerField()
    article_id = PositiveIntegerField()
