from django.apps import AppConfig


class SavedFeedConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app.saved_feed'
