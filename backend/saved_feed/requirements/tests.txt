pytest==6.2.4
pytest-django==4.4.0
pre-commit==2.13.0
pytest-socket==0.4.0  # Prevent HTTP requests during tests.
