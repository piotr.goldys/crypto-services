#!/bin/sh

set -o errexit
set -o nounset

# Wait for db to become available.
.scripts/wait_for_it.sh db:$MYSQL_TCP_PORT

# Perform database migrations and collectstatic before startup.
python3 /app/manage.py migrate
python3 /app/manage.py collectstatic --noinput

gunicorn app.django_settings.wsgi:application --bind 0.0.0.0:$PROJECT_PORT
