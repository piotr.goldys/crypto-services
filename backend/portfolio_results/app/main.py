import os

from app.calculator import PortfolioResultsCalculator
from app.coingecko_client import coingecko_client
from app.config import RedisConfig
from elasticapm.contrib.flask import ElasticAPM
from flask import Flask
from flask import jsonify
from flask import request
from flask_caching import Cache
from requests import HTTPError

app = Flask(__name__)

app.config.from_object(RedisConfig)
apm = ElasticAPM(app, service_name='portfolio_results', server_url=os.environ['APM_SERVER_URL'], logging=True)

cache = Cache(app)


@app.route('/')
def index():
    return 'Hello world'


@app.route('/get-coin-choices', methods=['GET'])
@cache.cached(timeout=60 * 60)
def get_coin_choices():
    choices = []
    try:
        coin_list_response = coingecko_client.get_coins_list()
    except HTTPError:
        return 'Too Many Requests', 429

    for coin_data in coin_list_response:
        choices.append(
            {'symbol': coin_data['symbol'].upper(), 'name': coin_data['name'], 'id': coin_data['id']}
        )

    return jsonify(
        {
            'choices': choices,
        }
    )


@app.route('/get-results', methods=['POST'])
def get_results():
    """
    Example request body:
    {
        "currency": "PLN",  # Uppercase
        "purchases": {
            "bitcoin": {
                "date": "2020-07-18",   # ISO format
                "amount_spent": 1000
            },
            "ethereum": {
                "date": "2019-07-18",
                "amount_spent": 1000
            }
        }
    }
    """
    return jsonify(
        PortfolioResultsCalculator(request.json['currency'], request.json['purchases']).calculate_results()
    )
