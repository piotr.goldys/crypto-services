from datetime import datetime

import requests
from app.coingecko_client import coingecko_client
from flask import request


class PortfolioResultsCalculator:
    def __init__(self, currency: str, purchases: list):
        self.currency = currency.upper()
        self.purchases = purchases

    def calculate_results(self) -> dict:
        total_result = 0
        current_total_value = 0
        for purchase_data in self.purchases:
            result, current_value = self.get_single_purchase_results(
                purchase_data['coin'], purchase_data['date'], purchase_data['amount_spent']
            )
            total_result += result
            current_total_value += current_value

        return {
            'currency': request.json['currency'],
            'result': round(total_result, 2),
            'current_value': round(current_total_value, 2),
        }

    def get_single_purchase_results(self, coin, date, amount_spent) -> tuple:
        # Get price of coin at given date.
        historical_price = self.get_historical_coin_price(coin, date)

        # Get current coin price.
        current_price = self.get_current_coin_price(coin)

        if self.currency != 'USD':
            historical_price = self.convert_from_usd(self.currency, historical_price, date)
            current_price = self.convert_from_usd(self.currency, current_price)

        # Get current value.
        price_multiplier = current_price / historical_price
        current_value = amount_spent * price_multiplier

        # Calculate profit/loss.
        result = current_value - amount_spent

        return result, current_value

    @staticmethod
    def get_historical_coin_price(coin, date) -> float:
        # Data format conversion for API client.
        date = datetime.fromisoformat(date).strftime('%d-%m-%Y')
        return coingecko_client.get_coin_history_by_id(coin, date, localization='false')['market_data'][
            'current_price'
        ]['usd']

    @staticmethod
    def get_current_coin_price(coin) -> float:
        return coingecko_client.get_price(coin, 'usd')[coin]['usd']

    @staticmethod
    def convert_from_usd(currency, amount, date='') -> float:
        json_body = {'currency': currency, 'amount': amount}
        if date:
            json_body['date'] = date

        return requests.post('http://currency_converter:83/convert-from-usd', json=json_body).json()
