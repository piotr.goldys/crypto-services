import os


class RedisConfig:
    CACHE_TYPE = 'RedisCache'
    CACHE_REDIS_HOST = 'redis'
    CACHE_REDIS_URL = os.environ['REDIS_URL']
