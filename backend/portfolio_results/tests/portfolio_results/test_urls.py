from app.main import app


def test_get_coin_choices_url():
    adapter = app.url_map.bind('')

    # Raise exception if endpoint is not available with specific method.
    adapter.match('/get-coin-choices', method='GET')


def test_get_results_url():
    adapter = app.url_map.bind('')

    # Raise exception if endpoint is not available with specific method.
    adapter.match('/get-results', method='POST')
