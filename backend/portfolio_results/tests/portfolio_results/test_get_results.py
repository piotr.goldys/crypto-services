from unittest.mock import patch

import responses
from app.main import PortfolioResultsCalculator


class TestPortfolioResultsCalculator:
    EXAMPLE_PURCHASES = [
        {'coin': 'bitcoin', 'date': '2018-07-18', 'amount_spent': 1000},
        {
            'coin': 'ethereum',
            'date': '2017-07-18',
            'amount_spent': 2000,
        },
    ]
    instance_1 = PortfolioResultsCalculator(currency='USD', purchases=EXAMPLE_PURCHASES)
    instance_2 = PortfolioResultsCalculator(currency='PLN', purchases=EXAMPLE_PURCHASES)

    def test_calculate_results(self, client):
        self.calculate_results_without_conversion(client, 'usd')
        self.calculate_results_without_conversion(client, 'USD')

        self.calculate_results_with_conversion(client, 'pln')
        self.calculate_results_with_conversion(client, 'PLN')

    @patch('pycoingecko.CoinGeckoAPI.get_coin_history_by_id')
    @patch('pycoingecko.CoinGeckoAPI.get_price')
    def calculate_results_without_conversion(self, client, currency, current_mock, historical_mock):
        assert currency.upper() == 'USD', 'Currency specified is other than USD. Conversion will occur.'

        historical_mock.side_effect = self.get_historical_mock_return_value
        current_mock.side_effect = self.get_current_mock_return_value

        response_data = client.post(
            '/get-results',
            json={
                'currency': currency,
                'purchases': self.EXAMPLE_PURCHASES,
            },
        ).json

        assert historical_mock.call_count == 2
        self.assert_results_without_conversion(response_data, currency)

    @responses.activate
    @patch('pycoingecko.CoinGeckoAPI.get_coin_history_by_id')
    @patch('pycoingecko.CoinGeckoAPI.get_price')
    def calculate_results_with_conversion(self, client, currency, current_mock, historical_mock):
        self.setup_conversion_responses()

        historical_mock.side_effect = self.get_historical_mock_return_value
        current_mock.side_effect = self.get_current_mock_return_value

        response_data = client.post(
            '/get-results', json={'currency': currency, 'purchases': self.EXAMPLE_PURCHASES}
        ).json

        assert historical_mock.call_count == 2

        calls = historical_mock.call_args_list
        assert ('bitcoin', '18-07-2018') == calls[0].args
        assert ('ethereum', '18-07-2017') == calls[1].args

        self.assert_results_with_conversion(response_data, currency)

    def assert_results_without_conversion(self, response_data, currency):
        current_value = response_data.get('current_value')
        result = response_data.get('result')

        current_value_btc = self.EXAMPLE_PURCHASES[0]['amount_spent'] * 5  # 5000
        result_btc = current_value_btc - self.EXAMPLE_PURCHASES[0]['amount_spent']  # 4000

        current_value_eth = self.EXAMPLE_PURCHASES[1]['amount_spent'] * 15  # 30000
        result_eth = current_value_eth - self.EXAMPLE_PURCHASES[1]['amount_spent']  # 28000

        assert current_value == current_value_btc + current_value_eth == 35000
        assert result == result_btc + result_eth == 32000

        assert response_data.get('currency') == currency

    def assert_results_with_conversion(self, response_data, currency):
        current_value = response_data.get('current_value')
        result = response_data.get('result')

        current_value_btc = self.EXAMPLE_PURCHASES[0]['amount_spent'] * 3.75  # 3750
        result_btc = current_value_btc - self.EXAMPLE_PURCHASES[0]['amount_spent']  # 2750

        current_value_eth = self.EXAMPLE_PURCHASES[1]['amount_spent'] * 11.25  # 22500
        result_eth = current_value_eth - self.EXAMPLE_PURCHASES[1]['amount_spent']  # 20500

        assert current_value == current_value_btc + current_value_eth == 26250
        assert result == result_btc + result_eth == 23250

        assert response_data.get('currency') == currency

    @staticmethod
    def get_historical_mock_return_value(coin, date, *args, **kwargs):
        if coin == 'bitcoin' and date == '18-07-2018':
            return {'id': 'bitcoin', 'market_data': {'current_price': {'usd': 10000}}}
        elif coin == 'ethereum' and date == '18-07-2017':
            return {'id': 'ethereum', 'market_data': {'current_price': {'usd': 200}}}

    @staticmethod
    def get_current_mock_return_value(coin, *args, **kwargs):
        if coin == 'bitcoin':
            return {'bitcoin': {'usd': 50000}}
        elif coin == 'ethereum':
            return {'ethereum': {'usd': 3000}}

    @staticmethod
    def setup_conversion_responses():
        # TODO: Store URLs to other services somewhere.
        responses.add(
            responses.POST,
            'http://currency_converter:83/convert-from-usd',
            status=200,
            match=[
                responses.json_params_matcher(
                    {
                        'currency': 'PLN',
                        'date': '2018-07-18',
                        'amount': 10000,
                    }
                )
            ],
            json=40000,
        )
        responses.add(
            responses.POST,
            'http://currency_converter:83/convert-from-usd',
            status=200,
            match=[responses.json_params_matcher({'currency': 'PLN', 'amount': 50000})],
            json=150000,
        )
        responses.add(
            responses.POST,
            'http://currency_converter:83/convert-from-usd',
            status=200,
            match=[responses.json_params_matcher({'currency': 'PLN', 'date': '2017-07-18', 'amount': 200})],
            json=800,
        )
        responses.add(
            responses.POST,
            'http://currency_converter:83/convert-from-usd',
            status=200,
            match=[responses.json_params_matcher({'currency': 'PLN', 'amount': 3000})],
            json=9000,
        )

    @patch('app.main.PortfolioResultsCalculator.get_historical_coin_price')
    @patch('app.main.PortfolioResultsCalculator.get_current_coin_price')
    def test_get_single_purchase_results_without_conversion(self, current_mock, historical_mock):
        historical_price = 10000
        current_price = 50000
        amount_spent = 1000
        value_multiplier = current_price / historical_price

        historical_mock.return_value = historical_price
        current_mock.return_value = current_price

        result, current_value = self.instance_1.get_single_purchase_results(
            'bitcoin', date='2020-07-18', amount_spent=amount_spent
        )

        assert current_value == 5000
        assert current_value == amount_spent * value_multiplier

        assert result == 4000
        assert result == amount_spent * value_multiplier - amount_spent

    @patch('app.main.PortfolioResultsCalculator.get_historical_coin_price')
    @patch('app.main.PortfolioResultsCalculator.get_current_coin_price')
    @patch('app.main.PortfolioResultsCalculator.convert_from_usd')
    def test_get_single_purchase_results_with_conversion(
        self, conversion_mock, current_mock, historical_mock
    ):
        historical_price = 10000
        current_price = 50000
        amount_spent = 1000
        value_multiplier = current_price / historical_price

        historical_mock.return_value = historical_price
        current_mock.return_value = current_price

        def convert_from_usd_return(currency, amount, date=None):
            if date:
                return amount * 4  # Historical price example: 4 PLN/USD
            else:
                return amount * 3  # Current price example: 3PLN/USD

        conversion_mock.side_effect = convert_from_usd_return

        result, current_value = self.instance_2.get_single_purchase_results(
            'bitcoin', date='2020-07-18', amount_spent=amount_spent
        )
        assert current_value == 3750
        assert current_value == amount_spent * value_multiplier * 3 / 4

        assert result == 2750
        assert result == current_value - amount_spent

    @patch('pycoingecko.CoinGeckoAPI.get_coin_history_by_id')
    def test_get_historical_coin_price(self, mock):
        mock.return_value = {
            'id': 'ethereum',
            'symbol': 'eth',
            'name': 'Ethereum',
            'market_data': {'current_price': {'pln': 689.6881628203627, 'usd': 188.3527773486568}},
        }

        assert self.instance_1.get_historical_coin_price('ethereum', '2020-07-18') == 188.3527773486568

        mock.assert_called_once()

        args, kwargs = mock.call_args
        for arg in ('ethereum', '18-07-2020'):
            assert arg in args

    @patch('pycoingecko.CoinGeckoAPI.get_price')
    def test_get_current_coin_price(self, mock):
        mock.return_value = {'bitcoin': {'usd': 1000}}

        assert self.instance_1.get_current_coin_price('bitcoin') == 1000

        mock.assert_called_once_with('bitcoin', 'usd')
