import json
from unittest.mock import patch


@patch('pycoingecko.CoinGeckoAPI.get_coins_list')
def test_get_coin_choices(mock, client):
    mock.return_value = [
        {'id': 'bitcoin', 'symbol': 'btc', 'name': 'Bitcoin'},
        {'id': 'ethereum', 'symbol': 'eth', 'name': 'Ethereum'},
    ]

    response = client.get('/get-coin-choices')

    mock.assert_called_once()
    assert json.loads(response.data) == {
        'choices': [
            {'symbol': 'BTC', 'name': 'Bitcoin', 'id': 'bitcoin'},
            {'symbol': 'ETH', 'name': 'Ethereum', 'id': 'ethereum'},
        ]
    }
