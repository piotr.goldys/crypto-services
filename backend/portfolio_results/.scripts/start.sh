#!/bin/sh

set -o errexit
set -o nounset

gunicorn app.main:app --bind 0.0.0.0:5000
