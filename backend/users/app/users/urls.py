from django.urls import path

from app.users.views import UserViewSet

urlpatterns = [
    path(
        'users',
        UserViewSet.as_view(
            {
                'get': 'list',
                'post': 'create',
            }
        ),
        name='users_list_create',
    ),
    path(
        'users/<int:pk>',
        UserViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'delete'}),
        name='users_retrieve_update_delete',
    ),
]
