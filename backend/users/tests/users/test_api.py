import logging

import pytest
from django.test import Client
from django.urls import reverse
from pytest import fixture
from rest_framework.status import HTTP_200_OK
from rest_framework.status import HTTP_201_CREATED
from rest_framework.status import HTTP_202_ACCEPTED
from rest_framework.status import HTTP_204_NO_CONTENT
from rest_framework.status import HTTP_404_NOT_FOUND

from app.users.models import User
from tests.users.constants import GET_CREATE_VIEW_NAME
from tests.users.constants import RETRIEVE_UPDATE_DELETE_VIEW_NAME

logger = logging.getLogger(__name__)


@pytest.mark.django_db
class TestUsersAPI:
    client = Client()
    test_username_1 = 'test_user_1'

    @fixture(scope='class')
    def create_user(self, django_db_setup, django_db_blocker):
        with django_db_blocker.unblock():
            assert not User.objects.exists(), 'No users should be stored in database at this point.'
            yield self.client.post(reverse(GET_CREATE_VIEW_NAME), data={'username': self.test_username_1})

    # Create
    def test_create(self, create_user):
        # User is created inside 'create_user' fixture.
        assert create_user.status_code == HTTP_201_CREATED
        assert create_user.data.get('username') == self.test_username_1
        assert User.objects.get(username=self.test_username_1)
        # TODO: User password should be required!

    # List
    def test_list(self):
        response = self.client.get(reverse(GET_CREATE_VIEW_NAME))
        assert response.status_code == HTTP_200_OK

        current_user_count = User.objects.count()
        assert current_user_count == len(response.data)

        assert response.data[0].get('id') == 1
        assert response.data[0].get('username') == self.test_username_1

        User.objects.create(username='test_user_2')
        current_user_count += 1

        response = self.client.get(reverse(GET_CREATE_VIEW_NAME))
        assert current_user_count == len(response.data)

    # Retrieve
    def test_retrieve(self):
        response = self.client.get(reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(1,)))
        assert response.status_code == HTTP_200_OK
        assert response.data.get('id') == 1
        assert response.data.get('username') == self.test_username_1

    def test_retrieve_not_found(self):
        # User ID does not exist.
        response = self.client.get(reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(2,)))
        assert response.status_code == HTTP_404_NOT_FOUND

    # Update
    def test_update(self):
        user = User.objects.get(pk=1)
        username_after = 'edited'

        response = self.client.put(
            reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(user.pk,)),
            data={'username': username_after},
            content_type='application/json',
        )
        assert response.status_code == HTTP_202_ACCEPTED
        assert response.data.get('username') == username_after
        assert User.objects.get(pk=user.pk).username == username_after

    def test_update_not_found(self):
        # User ID does not exist.
        response = self.client.put(
            reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(2,)),
            data={'username': 'whatever'},
            content_type='application/json',
        )
        assert response.status_code == HTTP_404_NOT_FOUND

    # Delete
    def test_delete(self):
        assert User.objects.get(pk=1)
        response = self.client.delete(reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(1,)))
        assert response.status_code == HTTP_204_NO_CONTENT
        with pytest.raises(User.DoesNotExist):
            User.objects.get(pk=1)

    def test_delete_not_found(self):
        # User ID does not exist.
        response = self.client.delete(reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(2,)))
        assert response.status_code == HTTP_404_NOT_FOUND
