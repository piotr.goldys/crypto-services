from django.urls import reverse

from tests.users.constants import GET_CREATE_VIEW_NAME
from tests.users.constants import RETRIEVE_UPDATE_DELETE_VIEW_NAME


class TestUserAPIURLs:
    """Test User API URL backward compatibility (make sure URLs don't change)."""

    # 'create' and 'list' views are under the same URL. They use different HTTP methods (GET and POST).
    def test_create_url(self):
        url = reverse(GET_CREATE_VIEW_NAME)
        assert url == '/users'

    def test_list_url(self):
        self.test_create_url()

    # 'retrieve', 'update' and 'delete' views are under the same URL.
    # They use different HTTP methods (PUT, UPDATE and DELETE).
    def test_retrieve_url(self):
        url = reverse(RETRIEVE_UPDATE_DELETE_VIEW_NAME, args=(1,))
        assert url == '/users/1'

    def test_update_url(self):
        self.test_retrieve_url()

    def test_delete_url(self):
        self.test_retrieve_url()
